import 'package:flutter/material.dart';
import 'package:flutter_clean_architecture/forth_page.dart';
import 'package:flutter_clean_architecture/state_container.dart';
import 'package:flutter_clean_architecture/user.dart';

class ThirdPage extends StatefulWidget {
  @override
  _ThirdPageState createState() => _ThirdPageState();
}

class _ThirdPageState extends State<ThirdPage> {
  User user;

  Widget get _userInfo {
    return Center(
        child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
          Text('This is the third page'),
          Text("${user.firstName} ${user.lastName}"),
          Text(user.email)
        ]));
  }

  Widget get _logInPrompt {
    return Center(child: Text('Please add user information'));
  }

  @override
  Widget build(BuildContext context) {
    final container = StateContainer.of(context);
    user = container.user;

    var body = user != null ? _userInfo : _logInPrompt;

    return Scaffold(
        appBar: AppBar(title: Text('Third Page')),
        body: body,
        floatingActionButton: FloatingActionButton(
            child: Icon(Icons.add),
            onPressed: () {
              var firstName = user.firstName;
              var lastName = user.lastName;
              var email = user.email;

              if (firstName == '') {
                firstName = null;
              }
              if (lastName == '') {
                lastName = null;
              }
              if (email == '') {
                email = null;
              }

              container.updateUserInfo(
                  firstName: firstName, lastName: lastName, email: email);

              Navigator.push(
                  context,
                  MaterialPageRoute(
                      fullscreenDialog: true,
                      builder: (context) {
                        return ForthPage();
                      }));
            }));
  }
}
