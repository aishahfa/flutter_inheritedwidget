import 'package:flutter/material.dart';
import 'package:flutter_clean_architecture/home.dart';
import 'package:flutter_clean_architecture/state_container.dart';

// https://ericwindmill.com/articles/inherited_widget/

void main() {
  runApp(StateContainer(child: MyApp()));
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: HomeScreen(),
    );
  }
}
