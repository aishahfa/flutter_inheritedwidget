import 'package:flutter/material.dart';
import 'package:flutter_clean_architecture/state_container.dart';
import 'package:flutter_clean_architecture/third_page.dart';

class UpdateUserScreen extends StatelessWidget {
  static final GlobalKey<FormState> formKey = GlobalKey<FormState>();

  static final GlobalKey<FormFieldState<String>> firstNameKey =
      GlobalKey<FormFieldState<String>>();
  static final GlobalKey<FormFieldState<String>> lastNameKey =
      GlobalKey<FormFieldState<String>>();
  static final GlobalKey<FormFieldState<String>> emailKey =
      GlobalKey<FormFieldState<String>>();

  const UpdateUserScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // get reference to your store
    final container = StateContainer.of(context);

    return Scaffold(
      appBar: AppBar(
        title: Text('Edit User Info'),
      ),
      body: Padding(
        padding: EdgeInsets.all(16.0),
        child: Form(
          key: formKey,
          autovalidate: false,
          child: ListView(
            children: [
              TextFormField(
                key: firstNameKey,
                style: TextStyle(fontSize: 40),
                decoration: InputDecoration(
                  hintText: 'First Name',
                ),
              ),
              TextFormField(
                key: lastNameKey,
                style: Theme.of(context).textTheme.headline,
                decoration: InputDecoration(
                  hintText: 'Last Name',
                ),
              ),
              TextFormField(
                key: emailKey,
                style: Theme.of(context).textTheme.headline,
                decoration: InputDecoration(
                  hintText: 'Email Address',
                ),
              )
            ],
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () {
          final form = formKey.currentState;
          if (form.validate()) {
            var firstName = firstNameKey.currentState.value;
            var lastName = lastNameKey.currentState.value;
            var email = emailKey.currentState.value;

            // This is a hack that isn't important
            // To this lesson. Basically, it prevents
            // The store from overriding user info
            // with an empty string if you only want
            // to change a single attribute
            if (firstName == '') {
              firstName = null;
            }
            if (lastName == '') {
              lastName = null;
            }
            if (email == '') {
              email = null;
            }

            // You can call the method from your store,
            // which will call set state and re-render
            // the widgets that rely on the user slice of state.
            // In this case, that's the home page
            container.updateUserInfo(
              firstName: firstName,
              lastName: lastName,
              email: email,
            );

//            Navigator.pop(context);
            Navigator.push(
                context,
                MaterialPageRoute(
                    fullscreenDialog: true,
                    builder: (context) {
                      return ThirdPage();
                    }));
          }
        },
      ),
    );
  }
}
