import 'package:flutter/material.dart';
import 'package:flutter_clean_architecture/state_container.dart';
import 'package:flutter_clean_architecture/user.dart';

class ForthPage extends StatefulWidget {
  @override
  _ForthPageState createState() => _ForthPageState();
}

class _ForthPageState extends State<ForthPage> {
  // Make a class property for the data you want
  User user;

  // This Widget will display the users info:
  Widget get _userInfo {
    return Center(
        child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
          // This refers to the user in your store
          Text('This is the forth page'),

          Text("${user.firstName} ${user.lastName}",
              style: TextStyle(fontSize: 24.0)),
          Text(user.email, style: TextStyle(fontSize: 24.0))
        ]));
  }

  Widget get _logInPrompt {
    return Center(
        child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
          Text('Please add user information',
              style: const TextStyle(fontSize: 18.0))
        ]));
  }

  @override
  Widget build(BuildContext context) {
    // This is how you access your store. This container
    // is where your properties and methods live
    final container = StateContainer.of(context);

    // set the class's user
    user = container.user;

    var body = user != null ? _userInfo : _logInPrompt;

    return Scaffold(
        appBar: AppBar(title: Text('Forth Page')),
        // The body will re-render to show user info
        // as its updated
        body: body);
  }
}
